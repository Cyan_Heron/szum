# import the necessary packages
from keras.models import load_model
import argparse
import pickle
import cv2
import os
import numpy as np
# construct the argument parser and parse the arguments

dim = 128

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True,	help="path to input image we are going to classify")
ap.add_argument("-m", "--model", required=True,	help="path to trained Keras model")
ap.add_argument("-l", "--label-bin", required=True,	help="path to label binarizer")
args = vars(ap.parse_args())

imagePath = args["image"]
label = imagePath.split(os.path.sep)[-2]
# load the image, resize the image to be 32x32 pixels (ignoring
# aspect ratio), flatten the image into 32x32x3=3072 pixel image
# into a list, and store the image in the data list
image = cv2.imread(imagePath)
image = cv2.resize(image, (dim, dim))
# data = image
# scale the raw pixel intensities to the range [0, 1]
data = np.array(image.flatten(), dtype="float") / 255.0
data = np.reshape(data, (1, dim, dim, 3))
labels = np.array(label)

# load the model and label binarizer
print("[INFO] loading network and label binarizer...")
model = load_model(args["model"])
lb = pickle.loads(open(args["label_bin"], "rb").read())
# make a prediction on the image
preds = model.predict(data)
# find the class label index with the largest corresponding
# probability
i = preds.argmax(axis=1)[0]
label = lb.classes_[i]

# draw the class label + probability on the output image
text = "{}: {:.2f}%".format(label, preds[0][i] * 100)
cv2.putText(image, text, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
# show the output image
cv2.imshow("Image", image)
cv2.waitKey(0)